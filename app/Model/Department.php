<?php
App::uses('AppModel', 'Model');
/**
 * Department Model
 *
 * @property Article $Article
 */
class Department extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'desc' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'department_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

        public function getDepartments(){
            $departments = $this->find('all', array('fields'=>array('id','title','desc'),'recursive'=>-1 ));
            $result = array();
            foreach ($departments as $department) {
                $result[count($result)] = $department ['Department'] ;
            }
            return  $result;
        }
        
         public function getUpdateDepartments($lastDate){
            $departments = $this->find('all', array('fields'=>array('id','title','desc'),'conditions'=>array('Department.modified >='=>$lastDate),'recursive'=>-1 ));
            $result = array();
            foreach ($departments as $department) {
                $result[count($result)] = $department ['Department'] ;
            }
            return  $result;
        }
        
}
