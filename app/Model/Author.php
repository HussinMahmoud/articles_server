<?php
App::uses('AppModel', 'Model');
/**
 * Author Model
 *
 * @property Article $Article
 */
class Author extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'name' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'email' => array(
			'email' => array(
				'rule' => array('email'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Article' => array(
			'className' => 'Article',
			'foreignKey' => 'author_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

        public function getAuthors(){
            $authors = $this->find('all', array('fields'=>array('id','name'),'recursive'=>-1 ));
            $result = array();
            foreach ($authors as $author) {
                $result[count($result)] = $author ['Author'];
            }
            return  $result;
        }
        
          public function getUpdateAuthors($lastDate){
            $authors = $this->find('all', array('fields'=>array('id','name'),'conditions'=>array('Author.modified >='=>$lastDate),'recursive'=>-1 ));
            $result = array();
            foreach ($authors as $author) {
                $result[count($result)] = $author ['Author'];
            }
            return  $result;
        }
        
}
