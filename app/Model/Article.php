<?php
App::uses('AppModel', 'Model');
/**
 * Article Model
 *
 * @property Author $Author
 * @property Department $Department
 */
class Article extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'title';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'title' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'author_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'department_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Author' => array(
			'className' => 'Author',
			'foreignKey' => 'author_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Department' => array(
			'className' => 'Department',
			'foreignKey' => 'department_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);

        public function getLastTwentyArticlesList(){
            $articles = $this->find('all',array('fields'=>array('Department.title','Author.name','Article.*'),'order' => 'Article.created DESC','limit'=>20));
            
           return $this->getHandleList($articles);
        }
        
        
          public function getUpdateArticlesList($lastDate){
            $articles = $this->find('all',array('fields'=>array('Department.title','Author.name','Article.*'),'conditions'=>array('Article.modified >='=>$lastDate),'order' => 'Article.created DESC'));
            
            return $this->getHandleList($articles);
        }
        
        public function getOlderId($id){
             $articles = $this->find('all',array('fields'=>array('Department.title','Author.name','Article.*'),'conditions'=>array('Article.id <'=>$id),'order' => 'Article.created DESC','limit'=>10));
            
            return $this->getHandleList($articles);
            
            
        }

                private function getHandleList($articles){
              $result = array();
            
            foreach ($articles as $article) {
                $article ['Article']['author_name'] = $article ['Author']['name'];
                $article ['Article']['department_title'] = $article ['Department']['title'];
                unset( $article ['Article']['article_url']);
                unset( $article ['Article']['content']);
                unset( $article ['Article']['modified']);
                unset( $article ['Author']);
                unset( $article ['Department']);
                
                $result[count($result)] = $article ['Article'] ;
            }
            return $result;
        }
       
        
        public function get_article_by_id ($id)  {

        $article = $this->find('first', array('fields' => array('Department.title', 'Author.name', 'Article.*'), 'conditions' => array('Article.id' => $id)));
        if (!key_exists('Article', $article)) {
            return NULL;
        }
        $article ['Article']['author_name'] = $article ['Author']['name'];
        $article ['Article']['department_title'] = $article ['Department']['title'];
        unset($article ['Article']['article_url']);
        unset($article ['Article']['modified']);
       unset($article ['Article']['desc']); 
        unset($article ['Author']);
        unset($article ['Department']);
        
        return $article['Article'];
    }

}
