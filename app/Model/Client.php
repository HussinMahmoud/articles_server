<?php
App::uses('AppModel', 'Model');
/**
 * Client Model
 *
 * @property Fetch $Fetch
 */
class Client extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */


	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasMany associations
 *
 * @var array
 */
	public $hasMany = array(
		'Fetch' => array(
			'className' => 'Fetch',
			'foreignKey' => 'client_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);
        public function createNewClient ($uid){
            $this->create();
            $this->set('uid',$uid);
            $this->set('num_fetch',1);
       
            return ($this->save());
             
        }
}
