<?php

App::uses('AppController', 'Controller');
App::uses('HttpSocket', 'Network/Http');
App::import('Model/Vos', 'ResponsManu');
App::import('Model/Vos', 'ResponsListArticles');
App::import('Model/Vos', 'ResponsArticle');
/**
 * Fetches Controller
 *
 * @property Fetch $Fetch
 * @property PaginatorComponent $Paginator
 */
class FetchesController extends AppController {

    /**
     * Components
     *
     * @var array
     */
    public $components = array('Paginator', 'RequestHandler');
    public $uses = array('Fetch', 'Setting', 'Client', 'Article', 'Department', 'Author');

    public function beforeFilter() {
        parent::beforeFilter();
        $this->Auth->allow('fetch_start','fetch_article_by_id','fetch_update','fetch_older_articles');
    }

    /**
     * index method
     *
     * @return void
     */
    public function index() {
        $this->Fetch->recursive = 0;
        $this->set('fetches', $this->Paginator->paginate());
        print_r($this->Author->getAuthors());
    }

    /**
     * view method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function view($id = null) {
        if (!$this->Fetch->exists($id)) {
            throw new NotFoundException(__('Invalid fetch'));
        }
        $options = array('conditions' => array('Fetch.' . $this->Fetch->primaryKey => $id));
        $this->set('fetch', $this->Fetch->find('first', $options));
    }

    /**
     * add method
     *
     * @return void
     */
    public function add() {
        if ($this->request->is('post')) {
            $this->Fetch->create();
            if ($this->Fetch->save($this->request->data)) {
                $this->Session->setFlash(__('The fetch has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The fetch could not be saved. Please, try again.'));
            }
        }
        $clients = $this->Fetch->Client->find('list');
        $this->set(compact('clients'));
    }

    /**
     * edit method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function edit($id = null) {
        if (!$this->Fetch->exists($id)) {
            throw new NotFoundException(__('Invalid fetch'));
        }
        if ($this->request->is(array('post', 'put'))) {
            if ($this->Fetch->save($this->request->data)) {
                $this->Session->setFlash(__('The fetch has been saved.'));
                return $this->redirect(array('action' => 'index'));
            } else {
                $this->Session->setFlash(__('The fetch could not be saved. Please, try again.'));
            }
        } else {
            $options = array('conditions' => array('Fetch.' . $this->Fetch->primaryKey => $id));
            $this->request->data = $this->Fetch->find('first', $options);
        }
        $clients = $this->Fetch->Client->find('list');
        $this->set(compact('clients'));
    }

    /**
     * delete method
     *
     * @throws NotFoundException
     * @param string $id
     * @return void
     */
    public function delete($id = null) {
        $this->Fetch->id = $id;
        if (!$this->Fetch->exists()) {
            throw new NotFoundException(__('Invalid fetch'));
        }
        $this->request->onlyAllow('post', 'delete');
        if ($this->Fetch->delete()) {
            $this->Session->setFlash(__('The fetch has been deleted.'));
        } else {
            $this->Session->setFlash(__('The fetch could not be deleted. Please, try again.'));
        }
        return $this->redirect(array('action' => 'index'));
    }

    public function fetch_start($key = 0, $client_id = 0) {
        $responseVo = null;

        if (!$client_id || $key !== $this->Setting->getKeyApp()) {
            return ($this->set('responseVo', null));
        }
         $find_client = $this->Client->find('first', array('conditions' => array('Client.uid' => $client_id), 'recursive' => -1));
          if(key_exists('Client', $find_client)){
                 return ($this->set('responseVo', null));
           }
        
        try {
            $client = $this->Client->createNewClient($client_id);
            $articles = $this->Article->getLastTwentyArticlesList();
            $departments = $this->Department->getDepartments();
            $authors = $this->Author->getAuthors();

            $responseVo = new ResponsManu($departments, $articles, $authors);
            $this->Fetch->create();
            $this->Fetch->set('client_id', $client['Client']['id']);
            $this->Fetch->set('response_json', json_encode($responseVo));
            $this->Fetch->set('fetch_type', 'first');
            $this->Fetch->save();


            return ($this->set('responseVo', $responseVo));
        } catch (Exception $exc) {
            //  echo $exc->getTraceAsString();
            return ($this->set('responseVo', null));
        }
    }

    public function fetch_update($client_uid = 0) {
        $responseVo = null;
        $clint_id = 0;
        $last_time_fetch = '';

        if (!$client_uid) {
            return ($this->set('responseVo', null));
        }

        //get client id , update counter fetch and auto updat last time fetch
        try {

            $client = $this->Client->find('first', array('conditions' => array('Client.uid' => $client_uid), 'recursive' => -1));
          if(!key_exists('Client', $client)){
                 return ($this->set('responseVo', null));
           }
            $client['Client']['num_fetch'] ++;
            $clint_id = $client['Client']['id'];
            $this->Client->save($client);
        } catch (Exception $exc) {
            return ($this->set('responseVo', null));
        }

        // last time fetch by client id 
        try {
            $fetch = $this->Fetch->find('first', array('conditions' => array('Fetch.client_id' => $clint_id, 'Fetch.fetch_type !=' => 'list', 'Fetch.article_id' => 0), 'order' => 'Fetch.created DESC', 'recursive' => -1));
            $last_time_fetch = $fetch['Fetch']['created'];
        } catch (Exception $exc) {
            return ($this->set('responseVo', null));
        }


        try {
            $deparments = $this->Department->getUpdateDepartments($last_time_fetch);
            $authors = $this->Author->getUpdateAuthors($last_time_fetch);
            $articles = $this->Article->getUpdateArticlesList($last_time_fetch);


            $responseVo = new ResponsManu($deparments, $articles, $authors);
            $this->Fetch->create();
            $this->Fetch->set('client_id', $clint_id);
            $this->Fetch->set('response_json', json_encode($responseVo));
            $this->Fetch->set('fetch_type', 'update');
            $this->Fetch->save();


            return ($this->set('responseVo', $responseVo));
        } catch (Exception $exc) {
            return ($this->set('responseVo', null));
        }
    }

    
    public function fetch_older_articles($client_uid = 0, $article_id = 0) {
        $responseVo = null;
        $clint_id = 0;

        if (!$client_uid || !$article_id) {
            return ($this->set('responseVo', null));
        }
        
        //get client id , update counter fetch and auto updat last time fetch
        try {

            $client = $this->Client->find('first', array('conditions' => array('Client.uid' => $client_uid), 'recursive' => -1));
           if(!key_exists('Client', $client)){
                 return ($this->set('responseVo', null));
           }
            $client['Client']['num_fetch'] ++;
            $clint_id = $client['Client']['id'];
            $this->Client->save($client);
        } catch (Exception $exc) {
            return ($this->set('responseVo', null));
        }
        
        try {
            $articles = $this->Article->getOlderId($article_id);
             $responseVo = new ResponsListArticles($articles);
            $this->Fetch->create();
            $this->Fetch->set('client_id', $clint_id);
            $this->Fetch->set('response_json', json_encode($responseVo));
            $this->Fetch->set('fetch_type', 'list');
            $this->Fetch->save();
             
              return ($this->set('responseVo', $responseVo));
             
        } catch (Exception $exc) {
             return ($this->set('responseVo', null));
        }
            
        
        
    }

     public function fetch_article_by_id($client_uid = 0, $article_id = 0) {
         $responseVo = null;
        $clint_id = 0;

        if (!$client_uid || !$article_id) {
            return ($this->set('responseVo', null));
        }
        
        //get client id , update counter fetch and auto updat last time fetch
        try {

            $client = $this->Client->find('first', array('conditions' => array('Client.uid' => $client_uid), 'recursive' => -1));
           if(!key_exists('Client', $client)){
                 return ($this->set('responseVo', null));
           }
            $client['Client']['num_fetch'] ++;
            $clint_id = $client['Client']['id'];
            $this->Client->save($client);
        } catch (Exception $exc) {
            return ($this->set('responseVo', null));
        }
         
         //get article by id 
      
           try {
           $article = $this->Article->get_article_by_id($article_id);
        
         if(!$article){
             return ($this->set('responseVo', null));
         }
             $responseVo = new ResponsArticle($article);
            $this->Fetch->create();
            $this->Fetch->set('client_id', $clint_id);
            $this->Fetch->set('article_id',$article_id);
            $this->Fetch->set('fetch_type', 'view');
            $this->Fetch->save();
             
              return ($this->set('responseVo', $responseVo));
             
        } catch (Exception $exc) {
             return ($this->set('responseVo', null));
        }
         
         
         
         
     }
    
    
}
