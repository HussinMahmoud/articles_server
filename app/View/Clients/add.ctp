<div class="clients form">
<?php echo $this->Form->create('Client'); ?>
	<fieldset>
		<legend><?php echo __('Add Client'); ?></legend>
	<?php
		echo $this->Form->input('uid');
		echo $this->Form->input('num_fetch');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Clients'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Fetches'), array('controller' => 'fetches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fetch'), array('controller' => 'fetches', 'action' => 'add')); ?> </li>
	</ul>
</div>
