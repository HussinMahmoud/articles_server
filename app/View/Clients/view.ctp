<div class="clients view">
<h2><?php echo __('Client'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($client['Client']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Uid'); ?></dt>
		<dd>
			<?php echo h($client['Client']['uid']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Num Fetch'); ?></dt>
		<dd>
			<?php echo h($client['Client']['num_fetch']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($client['Client']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($client['Client']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Client'), array('action' => 'edit', $client['Client']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Client'), array('action' => 'delete', $client['Client']['id']), null, __('Are you sure you want to delete # %s?', $client['Client']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Fetches'), array('controller' => 'fetches', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fetch'), array('controller' => 'fetches', 'action' => 'add')); ?> </li>
	</ul>
</div>
<div class="related">
	<h3><?php echo __('Related Fetches'); ?></h3>
	<?php if (!empty($client['Fetch'])): ?>
	<table cellpadding = "0" cellspacing = "0">
	<tr>
		<th><?php echo __('Id'); ?></th>
		<th><?php echo __('Client Id'); ?></th>
		<th><?php echo __('Response Json'); ?></th>
		<th><?php echo __('Request Json'); ?></th>
		<th><?php echo __('Created'); ?></th>
		<th><?php echo __('Modified'); ?></th>
		<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($client['Fetch'] as $fetch): ?>
		<tr>
			<td><?php echo $fetch['id']; ?></td>
			<td><?php echo $fetch['client_id']; ?></td>
			<td><?php echo $fetch['response_json']; ?></td>
			<td><?php echo $fetch['request_json']; ?></td>
			<td><?php echo $fetch['created']; ?></td>
			<td><?php echo $fetch['modified']; ?></td>
			<td class="actions">
				<?php echo $this->Html->link(__('View'), array('controller' => 'fetches', 'action' => 'view', $fetch['id'])); ?>
				<?php echo $this->Html->link(__('Edit'), array('controller' => 'fetches', 'action' => 'edit', $fetch['id'])); ?>
				<?php echo $this->Form->postLink(__('Delete'), array('controller' => 'fetches', 'action' => 'delete', $fetch['id']), null, __('Are you sure you want to delete # %s?', $fetch['id'])); ?>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php endif; ?>

	<div class="actions">
		<ul>
			<li><?php echo $this->Html->link(__('New Fetch'), array('controller' => 'fetches', 'action' => 'add')); ?> </li>
		</ul>
	</div>
</div>
