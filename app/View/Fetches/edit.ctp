<div class="fetches form">
<?php echo $this->Form->create('Fetch'); ?>
	<fieldset>
		<legend><?php echo __('Edit Fetch'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('client_id');
		echo $this->Form->input('response_json');
		echo $this->Form->input('request_json');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Fetch.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Fetch.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Fetches'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
	</ul>
</div>
