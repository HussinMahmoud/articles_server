<div class="fetches view">
<h2><?php echo __('Fetch'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($fetch['Fetch']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Client'); ?></dt>
		<dd>
			<?php echo $this->Html->link($fetch['Client']['id'], array('controller' => 'clients', 'action' => 'view', $fetch['Client']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Response Json'); ?></dt>
		<dd>
			<?php echo h($fetch['Fetch']['response_json']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Request Json'); ?></dt>
		<dd>
			<?php echo h($fetch['Fetch']['request_json']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($fetch['Fetch']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($fetch['Fetch']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Fetch'), array('action' => 'edit', $fetch['Fetch']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Fetch'), array('action' => 'delete', $fetch['Fetch']['id']), null, __('Are you sure you want to delete # %s?', $fetch['Fetch']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Fetches'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Fetch'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Clients'), array('controller' => 'clients', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Client'), array('controller' => 'clients', 'action' => 'add')); ?> </li>
	</ul>
</div>
