<?php
App::uses('AppModel', 'Model');
/**
 * Machine Model
 *
 * @property Police $Police
 */
class Machine extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'number_voters' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'females' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'males' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'turnout' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'number_turnout' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'solid_votes' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'to_success' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'volunteers' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'police_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'user_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * hasOne associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Police' => array(
			'className' => 'Police',
			'foreignKey' => 'police_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
            'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
	);
        
        
        
       public function createMachine($publicId, $userId) {
        $this->create();
        $this->set('police_id', $publicId);
        $this->set('user_id', $userId);
        return $this->save();
    }

    public function getByPoliceIdOrCreateNew($policeId) {
        $machine = $this->find('first', array('conditions' => array('Machine.police_id' => $policeId)));
        if (!array_key_exists('Machine', $machine)) {
             $machine = $this->createMachine($policeId, 0);
        } 

        return $machine['Machine'];
    }
     public function getNumberVolunteerByPoliceId($policeId) {
          $machine = $this->find('first', array('conditions' => array('Machine.police_id' => $policeId)));
          if (!array_key_exists('Machine', $machine)) {
              return 0;
        } 
        return $machine['Machine']['volunteers'] ;
          
     }
   
        public function getTotalStatistics($city_id){
            $statistics = $this->find('first',array('joins' => array(array('table' => 'polices',
                                   'alias' => 'Police',
                                   'type' => 'INNER',
                                   'conditions' => array('Machine.police_id = Police.id')))
				,'fields'=>array(' COUNT(Machine.police_id) AS Cpolices','SUM(Machine.number_voters) AS Tvoters','AVG(Machine.turnout) AS Aturnout','SUM(Machine.solid_votes) AS Tsolid','SUM(Machine.to_success) AS Tsuccess','SUM(Machine.volunteers) AS Tvolunteers')
                ,'conditions' => array('Police.city_id'=>$city_id,'Machine.number_voters !='=>0)
                ,     'group'=>array('Police.city_id'),
                 'recursive'=>-1                        
                ));
            if (!array_key_exists(0, $statistics)){
              return 0 ;  
            }
                return $statistics [0];
        }
        
        public function getTotalStatisticsByConstituencyId($constituency_id){
            $statistics = $this->find('first',array('joins' => array(array('table' => 'polices',
                                   'alias' => 'Police',
                                   'type' => 'INNER',
                                   'conditions' => array('Machine.police_id = Police.id')))
				,'fields'=>array(' COUNT(Machine.police_id) AS Cpolices','SUM(Machine.number_voters) AS Tvoters','AVG(Machine.turnout) AS Aturnout','SUM(Machine.solid_votes) AS Tsolid','SUM(Machine.to_success) AS Tsuccess','SUM(Machine.volunteers) AS Tvolunteers')
                ,'conditions' => array('Police.constituency_id'=>$constituency_id,'Machine.number_voters !='=>0)
                ,     'group'=>array('Police.city_id'),
                 'recursive'=>-1                        
                ));
            if (!array_key_exists(0, $statistics)){
              return 0 ;  
            }
                return $statistics [0];
        }
        
        public function newMachinesByCity($city_id){
            $statistics = $this->find('all',array('joins' => array(array('table' => 'polices',
                                   'alias' => 'Police',
                                   'type' => 'INNER',
                                   'conditions' => array('Machine.police_id = Police.id')))
				,'fields'=>array('Police.name','Machine.number_voters','Machine.turnout','Machine.solid_votes','Machine.to_success','Machine.volunteers')
                ,'conditions' => array('Police.city_id'=>$city_id,'Machine.number_voters !='=>0)
                ,
                  'recursive'=>-1 , 'limit' => 10, 'order' => 'Machine.modified'
                                       
                ));
            if (!array_key_exists(0, $statistics)){
              return 0 ;  
            }
                return $statistics;
            
        }
        
        
        
}
