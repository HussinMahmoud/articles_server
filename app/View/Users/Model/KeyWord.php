<?php
App::uses('AppModel', 'Model');
/**
 * KeyWord Model
 *
 */
class KeyWord extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'source_name';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'source_name' => array(
			'notEmpty' => array(
				'rule' => 'isUnique',
				'message' => 'Your custom message here',
				'allowEmpty' => false,
				'required' => true,
				 'on' => 'create',
				'last' => false, // Stop validation after this rule
				'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'en_display' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'ar_display' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		)
		//,
		// 'model_class' => array(
		// 	'notEmpty' => array(
		// 		'rule' => array('notEmpty'),
		// 		//'message' => 'Your custom message here',
		// 		//'allowEmpty' => false,
		// 		//'required' => false,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
	);

public function getListByModel($model,$lang){

	if ($lang) {
		$lang = 'en_display';
	}else{
		$lang = 'ar_display';
	}
	$conditions =  array('model_class' =>array('' , $model) );

$KeyWord = $this->find('list',array('fields' => array('source_name',$lang), 'conditions' => $conditions));

return $KeyWord ;


}




}
