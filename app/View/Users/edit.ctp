﻿<div class="users form" >
    <?php echo $this->Form->create('User'); ?>
    <fieldset >
        <legend><?php// echo __('Edit User'); ?></legend>
        <?php
        
        echo _('<h2>'.'تعديل حساب المستخدم  '.'</br>'.$this->request->data['User']['username'].'</h2>');
        echo $this->Form->input('id');
        echo $this->Form->input('username',array('type'=>'hidden'));
        echo $this->Form->input('email',array('label'=>'البريد الإلكترونى'));
        echo $this->Form->input('password_update', array('label' => 'كلمة المرور الجديدة فى حالة الرغبة فى تغير كلمة المرور', 'maxLength' => 255, 'type' => 'password', 'required' => 0));
        echo $this->Form->input('password_confirm_update', array('label' => 'أعد إدخال كلمة المرور', 'maxLength' => 255, 'title' => 'Confirm New password', 'type' => 'password', 'required' => 0));
        echo $this->Form->input('tel',array('label'=>'رقم الهاتف'));
        
       
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>

      
        <li><?php echo $this->Html->link(__('List Users'), array('action' => 'index')); ?></li>
    </ul>
</div>
