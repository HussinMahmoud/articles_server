<div class="articles index">
	<h2><?php echo __('المقالات'); ?></h2>
        <table cellpadding="0" cellspacing="0" dir="rtl">
	<tr>
			<th><?php echo $this->Paginator->sort('id','كود المقال'); ?></th>
			<th><?php echo $this->Paginator->sort('title','عنوان المقال'); ?></th>
			<th><?php echo $this->Paginator->sort('author_id','المؤلف'); ?></th>
			<th><?php echo $this->Paginator->sort('department_id','القسم'); ?></th>		
			<th><?php echo $this->Paginator->sort('created','تحرير فى'); ?></th>
			<th><?php echo $this->Paginator->sort('modified','تعديل فى'); ?></th>
			<th class="actions"><?php echo __('إجراءات'); ?></th>
	</tr>
	<?php foreach ($articles as $article): ?>
	<tr>
		<td><?php echo h($article['Article']['id']); ?>&nbsp;</td>
		<td><?php echo h($article['Article']['title']); ?>&nbsp;</td>               
		<td>
			<?php echo $this->Html->link($article['Author']['name'], array('controller' => 'authors', 'action' => 'view', $article['Author']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($article['Department']['title'], array('controller' => 'departments', 'action' => 'view', $article['Department']['id'])); ?>
		</td>
	
		
		<td><?php echo h($article['Article']['created']); ?>&nbsp;</td>
		<td><?php echo h($article['Article']['modified']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('عرض'), array('action' => 'view', $article['Article']['id'])); ?>
			<?php echo $this->Html->link(__('تحرير'), array('action' => 'edit', $article['Article']['id'])); ?>
			<?php echo $this->Form->postLink(__('حذف'), array('action' => 'delete', $article['Article']['id']), null, __('Are you sure you want to delete # %s?', $article['Article']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('السابق'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('التالى') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('مهام'); ?></h3>
        <ul>

            <li><?php echo $this->Html->link(__('مقال جديد'), array('action' => 'add')); ?> </li>
            <li><?php echo $this->Html->link(__('قائمة المقالات'), array('action' => 'index')); ?></li>
            <li><?php echo $this->Html->link(__('قائمة المؤلفين'), array('controller' => 'authors', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('أضف مؤلف جديد'), array('controller' => 'authors', 'action' => 'add')); ?> </li>
            <li><?php echo $this->Html->link(__('قائمة الاقسام'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('أضف قسم جديد'), array('controller' => 'departments', 'action' => 'add')); ?> </li>	
        </ul>
</div>
