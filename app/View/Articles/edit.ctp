<div class="articles form">
    <?php echo $this->Form->create('Article'); ?>
    <fieldset>
        <legend><?php echo __('تعديل مقال'); ?></legend>
        <?php
        echo $this->Form->input('id');
        echo $this->Form->input('title', array('label' => 'عنوان المقال'));
        echo $this->Form->input('author_id', array('label' => 'المؤلف'));
        echo $this->Form->input('department_id', array('label' => 'القسم'));
        echo $this->Form->input('article_url', array('label' => 'رابط المقال'));
        echo $this->Form->input('photo_url', array('label' => 'أضف الصورة الرئيسية للمقال'));
        echo $this->Form->input('content', array('label' => 'اضف المحتوى هنا '));
        ?>
    </fieldset>
    <?php echo $this->Form->end(__('نشر التعديل')); ?>
</div>
<div class="actions">
    <h3><?php echo __('مهام'); ?></h3>
    <ul>
        <li><?php echo $this->Form->postLink(__('حذف المقال'), array('action' => 'delete', $this->Form->value('Article.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Article.id'))); ?></li>
        <li><?php echo $this->Html->link(__('قائمة المقالات'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('قائمة المؤلفين'), array('controller' => 'authors', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('أضف مؤلف جديد'), array('controller' => 'authors', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('قائمة الاقسام'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('أضف قسم جديد'), array('controller' => 'departments', 'action' => 'add')); ?> </li>
    </ul>

</div>
