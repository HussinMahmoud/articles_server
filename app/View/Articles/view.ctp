<div class="articles view">
    <h2><?php echo __('Article'); ?></h2>
    <dl>
        <dt><?php echo __('Id'); ?></dt>
        <dd>
            <?php echo h($article['Article']['id']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('عنوان المقال'); ?></dt>
        <dd>
            <?php echo h($article['Article']['title']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('المؤلف'); ?></dt>
        <dd>
            <?php echo $this->Html->link($article['Author']['name'], array('controller' => 'authors', 'action' => 'view', $article['Author']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('القسم'); ?></dt>
        <dd>
            <?php echo $this->Html->link($article['Department']['title'], array('controller' => 'departments', 'action' => 'view', $article['Department']['id'])); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('رابط المقال'); ?></dt>
        <dd>
            <?php echo h($article['Article']['article_url']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('الصورة الرئيسية للمقال'); ?></dt>
        <dd>
            <?php echo h($article['Article']['photo_url']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('المحتوى'); ?></dt>
        <dd>
            <?php echo h($article['Article']['content']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('تحرير فى'); ?></dt>
        <dd>
            <?php echo h($article['Article']['created']); ?>
            &nbsp;
        </dd>
        <dt><?php echo __('اخر تعديل'); ?></dt>
        <dd>
            <?php echo h($article['Article']['modified']); ?>
            &nbsp;
        </dd>
    </dl>
</div>
<div class="actions">
    <h3><?php echo __('Actions'); ?></h3>
    <ul>
        <li><?php echo $this->Html->link(__('تعديل المقال'), array('action' => 'edit', $article['Article']['id'])); ?> </li>
        <li><?php echo $this->Form->postLink(__('حذف المقال'), array('action' => 'delete', $article['Article']['id']), null, __('Are you sure you want to delete # %s?', $article['Article']['id'])); ?> </li>
        <li><?php echo $this->Html->link(__('مقال جديد'), array('action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('قائمة المقالات'), array('action' => 'index')); ?></li>
        <li><?php echo $this->Html->link(__('قائمة المؤلفين'), array('controller' => 'authors', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('أضف مؤلف جديد'), array('controller' => 'authors', 'action' => 'add')); ?> </li>
        <li><?php echo $this->Html->link(__('قائمة الاقسام'), array('controller' => 'departments', 'action' => 'index')); ?> </li>
        <li><?php echo $this->Html->link(__('أضف قسم جديد'), array('controller' => 'departments', 'action' => 'add')); ?> </li>

    </ul>
</div>
