<?php
App::uses('Fetch', 'Model');

/**
 * Fetch Test Case
 *
 */
class FetchTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.fetch',
		'app.client'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Fetch = ClassRegistry::init('Fetch');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Fetch);

		parent::tearDown();
	}

}
